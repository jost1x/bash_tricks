#!/bin/bash

echo "########################################################"
echo "Proceso 1. Instalando git."
echo "########################################################"

sudo apt-get install git

echo "########################################################"
echo "Proceso 2. Instalando librerias Python 3."
echo "########################################################"

sudo pip3 install paho-mqtt
sudo pip3 install minimalmodbus
sudo pip3 install RPi.GPIO


echo "########################################################"
echo "Proceso 3. Descargando codigo desde los servidores."
echo "Se solicitara el acceso."
echo "########################################################"

rm -r -f ~/myenergy
git clone https://gitlab.com/Domergy/myenergy ~/myenergy
chmod +x ~/myenergy/start.sh

echo "########################################################"
echo "Proceso 4. Configuracion RASPBERRY"
echo "########################################################"

/usr/bin/python3 ~/myenergy/install.py

echo "########################################################"
echo "Proceso 5. Instalación de Servicio."
echo "########################################################"


sudo systemctl stop myenergy.service
sudo mv -f ~/myenergy/service/myenergy.service /etc/systemd/system/
sudo systemctl enable myenergy.service
sudo systemctl start myenergy.service


echo "########################################################"
echo "Completado. Reiniciando dispositivo."
echo "########################################################"

shutdown -r +1
